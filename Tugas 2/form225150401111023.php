<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Form</title>
    </head>

    <body>
        <h1>Form Dept. Sistem Informasi</h1>

        <form action="form225150401111023.php" method="GET">
            <label for="nama">Nama:</label>
            <input type="text" id="nama" name="nama"><br><br>

            <label for="nim">NIM:</label>
            <input type="text" id="nim" name="nim"><br><br>

            <label for="jenis_kelamin">Jenis Kelamin:</label><br>
            <input type="radio" id="laki" name="jenis_kelamin" value="Laki-laki">
            <label for="laki">Laki-laki</label><br>
            <input type="radio" id="perempuan" name="jenis_kelamin" value="Perempuan">
            <label for="perempuan">Perempuan</label><br><br>

            <label for="program studi">Program Studi:</label><br>
            <select id="program_studi" name="program_studi">
                <option value="_">Pilih Satu</option>
                <option value="Sistem Informasi">Sistem Informasi</option>
                <option value="Teknologi Informasi">Teknologi Informasi</option>
                <option value="Pendidikan Teknologi Informasi">Pendidikan Teknologi Informasi</option>
            </select><br><br>

            <label for="tempat_tinggal">Tempat Tinggal:</label><br>
            <select id="tempat_tinggal" name="tempat_tinggal">
                <option value="_">Pilih Satu</option>
                <option value="Rumah">Rumah</option>
                <option value="Kost">Kost</option>
                <option value="Kontrak">Kontrak</option>
            </select><br><br>

            <input type="submit" value="Submit">
        </form>
        <br>

        <?php
            if (isset($_GET['nama']) && isset($_GET['nim']) && isset($_GET['jenis_kelamin']) && isset($_GET['tempat_tinggal'])) {
                $nama = $_GET['nama'];
                $nim = $_GET['nim'];
                $jenis_kelamin = $_GET['jenis_kelamin'];
                $program_studi = $_GET['program_studi'];
                $tempat_tinggal = $_GET['tempat_tinggal'];
            
                echo "<table border=1>";
                echo "<tr><td>Nama</td>";
                echo "<td>", $nama, "</td></tr>";
                echo "<tr><td>NIM</td>";
                echo "<td>", $nim, "</td></tr>";
                echo "<tr><td>Jenis Kelamin</td>";
                echo "<td>", $jenis_kelamin, "</td></tr>";
                echo "<tr><td>Program Studi</td>";
                echo "<td>", $program_studi, "</td></tr>";
                echo "<tr><td>Tempat Tinggal</td>";
                echo "<td>", $tempat_tinggal, "</td></tr>";
                echo "</table>";
            } else {
                echo "<h2>Isi form terlebih dahulu.</h2>";
            }        
        ?>
        <br>
    </body>
</html>